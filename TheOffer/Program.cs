﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TheOffer
{
    class Program
    {
        static void Main(string[] args)
        {
            
            Console.WriteLine("Введите текст");
            string textIn = Convert.ToString(Console.ReadLine());
            Console.WriteLine(Transform(textIn));
            Console.ReadLine();
        }

        private static string Transform(string input)
        {
            StringBuilder builder = new StringBuilder();
            foreach (char t in input)
            {
                builder.Append(char.ToLower(t));
            }

            builder[0] = char.ToUpper(builder[0]);

            for(int x = 0; x < builder.Length - 2; x++)
            {
                while ((builder[x] == ' ') && ((builder[x + 1] == ' ') || (builder[x + 1] == '.') || (builder[x + 1] == ',') || (builder[x + 1] == '?') || (builder[x + 1] == '!')))
                { //Исключение ненужных пробелов.  
                    builder.Remove(x, 1);
                }

                if ((builder[x] == '.' || builder[x] == ',' || (builder[x] == '?') || (builder[x] == '!')) && (builder[x + 1] != ' '))
                {// Добавление после точки или запятой пробела при его отсутствии
                    builder.Insert(x + 1, ' ');
                }

                if (builder[x] == '.' || (builder[x] == '?') || (builder[x] == '!'))
                {// Перевод в верхний регистр начало предложений
                    builder[x + 2] = char.ToUpper(builder[x + 2]);
                }
            }

            return builder.ToString();
        }
       
    }
}
